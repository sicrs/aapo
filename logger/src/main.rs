mod proto;
use proto::log_capnp;

use capnp::capability::Promise;
use capnp_rpc::{rpc_twoparty_capnp, twoparty, RpcSystem};

struct LoggerClient {}

impl log_capnp::logger::Server for LoggerClient {
    fn log(
        &mut self, 
        params: log_capnp::logger::LogParams,
        _: log_capnp::logger::LogResults
    ) -> capnp::capability::Promise<(), capnp::Error> { 
        let param = params.get().unwrap();
        let source = param.get_source().unwrap();
        let message = param.get_message().unwrap();
        println!("[{}]: {}", String::from(source), String::from(message));
        Promise::ok(())
    }
    
}

fn main() {
    async_std::task::block_on(async move {
        let listener = async_std::net::TcpListener::bind("127.0.0.1:8091").await.unwrap();
        let cli: proto::log_capnp::logger::Client = capnp_rpc::new_client(LoggerClient {});
        loop {
            let (stream, _) = listener.accept().await.unwrap();
            stream.set_nodelay(true).unwrap();
            let (reader, writer) = stream.split();
            let network = twoparty::VatNetwork::new(
                reader,
                writer,
                rpc_twoparty_capnp::Side::Server,
                Default::default(),
            );

            let rpc_system = RpcSystem::new(Box::new(network), Some(cli.clone().client));
            async_std::task::spawn(rpc_system.map(|_| ()));
        }
    });
}
