package main

import (
	"encoding/json"
	"fmt"
	"net"
	"os"

	"github.com/sicrs/gatekeeper/microservices/payment/payment"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/paymentintent"
	"zombiezen.com/go/capnproto2/rpc"
)

// a local implementation of InvokePayment
type invokePayment struct{}

func (inv invokePayment) CallPayment(call payment.InvokePayment_callPayment) error {
	amt := call.Params.Amount()
	params := &stripe.PaymentIntentParams{
		Amount:   stripe.Int64(amt),
		Currency: stripe.String(string(stripe.CurrencyIDR)),
	}

	pi, err := paymentintent.New(params)
	if err != nil {
		fmt.Fprintln(os.Stderr, "yikes")
		os.Exit(1)
	}

	buf, err := toJSON(struct {
		ClientSecret string `json:"clientSecret"`
	}{
		ClientSecret: pi.ClientSecret,
	})

	return call.Results.SetResponse(string(buf))
}

func toJSON(v interface{}) ([]byte, error) {
	content, err := json.Marshal(v)
	return content, err
}

func handle(conn net.Conn) error {
	main := payment.InvokePayment_ServerToClient(invokePayment{})
	connec := rpc.NewConn(rpc.StreamTransport(conn), rpc.MainInterface(main.Client))
	err := connec.Wait()
	return err
}

func main() {
	fmt.Fprintln(os.Stdout, "Listening on port 8090")
	listener, err := net.Listen("tcp", ":8090")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			go handle(conn)
		}
	}
}
