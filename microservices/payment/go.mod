module github.com/sicrs/gatekeeper/microservices/payment

go 1.14

require (
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/stripe/stripe-go v70.15.0+incompatible
	github.com/tinylib/msgp v1.1.2 // indirect
	golang.org/x/net v0.0.0-20200822124328-c89045814202
	zombiezen.com/go/capnproto2 v2.18.0+incompatible
)
