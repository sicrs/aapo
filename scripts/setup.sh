#!/bin/bash
echo "Running setup.sh..."
echo "Installing capnpc-go"
go install zombiezen.com/go/capnproto2/capnpc-go

echo "Installing capnpc-rust"
cargo install capnpc

source ~/.cargo/env
PATH=$PATH:$HOME/go/bin

echo "setup complete."