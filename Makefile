all: main
	cargo run

proto_payment: proto_payment_rust proto_payment_go

proto_payment_go:
	capnp compile -I ${GOPATH}/src/zombiezen.com/go/capnproto2/std proto/payment_go.capnp -ogo:microservices/payment/payment --src-prefix proto

proto_payment_rust:
	capnp compile proto/payment.capnp -orust:src/payment/ --src-prefix proto 

proto_logger: proto_logger_rust proto_logger_go

proto_logger_rust:
	capnp compile proto/log.capnp -orust:logger/src/proto --src-prefix proto

proto_logger_go:
	capnp compile -I ${GOPATH}/src/zombiezen.com/go/capnproto2/std proto/log_go.capnp -ogo:microservices/common/logger --src-prefix proto

main:
	cargo build --release

debug:
	cargo build