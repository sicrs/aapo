#[allow(dead_code)]
pub mod payment_capnp;
use payment_capnp::invoke_payment;

pub struct PaymentStub {
    inner: invoke_payment::Client,
    
}