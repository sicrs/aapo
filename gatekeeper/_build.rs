extern crate capnpc;

fn main() {
    capnpc::CompilerCommand::new()
        .src_prefix("proto")
        .file("../proto/payment.capnp")
        .output_path("src/payment/")
        .run()
        .unwrap();
}