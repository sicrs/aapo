using Go = import "/go.capnp";
@0xdc5b2cc8b1e3c4f7;
$Go.package("payment");
$Go.import("payment");

interface InvokePayment {
    callPayment @0 (amount: Int64) -> (response: Text);
}