using Go = import "/go.capnp";
@0xa7723f0726522f4b;
$Go.package("logger");
$Go.import("logger");

interface Logger {
    log @0 (source: Text, message: Text);
}